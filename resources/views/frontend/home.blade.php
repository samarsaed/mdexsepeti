@extends('frontend.layouts.app')

@section('content')

    @push('after-styles')
        <style>
            .quantity .pro-qty .Qtybtn {
                font-size: 24px;
                color: #fff;
                float: left;
                line-height: 27px;
                cursor: pointer;
                width: 18px;
            }

            .quantity .pro-qty .Qtybtn.dec {
                font-size: 30px;
            }
            .top_header .search .input-group-addon{
                padding: 11px 20px;
                padding-left: 36px;

            }
            #scroll-btn {
                padding-top: 10px;
            }
        </style>

    @endpush

    <!-- start slider -->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-pause="false">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
       

            @foreach($data["slides"] as $key => $slide)
                <div class="item {{$key == 0 ? "active" : ""}}">
                    <img style="width: 100%" src="/storage/{{$slide->slide}}" class="img-responsive" alt="...">
                </div>
            @endforeach

        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
    <!-- end slider -->
    <!-- start special offers -->
    <div class="special_offers">
        <div class="container">

            <div class="row">
                <div class="col-xs-10">
                    <div style="display:none" class="alert alert-danger" id="err"></div>
                    <div style="display:none" class="alert alert-success" id="succ"></div>
                    <h3>العروض المميزة</h3>
                </div>
               
                <div class="col-xs-12" style="direction: ltr;">
                    <div class="owl-carousel owl-theme">
                        @if(isset($data['offers']))
                            @foreach($data['offers'] as $offer)
                                <div class="item">
                                    <!-- <div class="overlay-out">
                                         <span> نفذت الكمية</span>
                                         <a href="#"> نبهني عندما يكون المنتج متاح</a>
                                    </div> -->
                                    <span class="padge-r">
                                خصم {{$offer->discount_percentage}}%
                            </span>
                                    @if(Auth::guard('customer')->check())
                                        <i class="fa fa-heart padge-l offerFav{{$offer->id}}"
                                           style="color:{{$offer->is_favourite != true ? "" : "red"}}"
                                           data-id="{{$offer->id}}"></i>
                                    @else
                                            <i class="fa fa-heart padge-l offerFav{{$offer->id}}"
                                               data-id="{{$offer->id}}"></i>
                                       
                                    @endif

                                   
                                        <img
                                            src="{{$offer->default_image }}"
                                            class="img-responsive center-block" alt="">
                                        <h4>{{$offer->title}}</h4>


                                        <p>
                                             <span>
                                                {{$offer->price_after_discount}}
                                             </span>ريال

                                        <p class="small-p">
                                            <del> {{$offer->price}} </del>
                                            ريال
                                        </p>

                                        </p>
                                       
                                    
                              
                    
                                </div>
                            @endforeach
                        @endif

                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- end special offers -->


    <!-- start recently arrived-->
    <div class="special_offers">
        <div class="container">

            <div class="row">
                <div class="col-xs-10">
                    <div style="display:none" class="alert alert-danger" id="err"></div>
                    <div style="display:none" class="alert alert-success" id="succ"></div>
                    <h3>وصل حديثاً</h3>
                </div>
              
                <div class="col-xs-12" style="direction: ltr;">
                    <div class="owl-carousel owl-theme">
                        @if(isset($data['latestProducts']))
                            @foreach($data['latestProducts'] as $latestProduct)
                                <div class="item">
                                    <!-- <div class="overlay-out">
                                         <span> نفذت الكمية</span>
                                         <a href="#"> نبهني عندما يكون المنتج متاح</a>
                                    </div> -->
                                    @if($latestProduct->discount_percentage != 0)
                                        <span class="padge-r">
                                 خصم {{$latestProduct->discount_percentage}}%
                                </span>
                                    @endif
                                    @if(Auth::guard('customer')->check())
                                        <i class="fa fa-heart padge-l offerFav{{$latestProduct->id}}"
                                           style="color:{{$latestProduct->is_favourite != true ? "" : "red"}}"
                                           data-id="{{$latestProduct->id}}"></i>
                                    @else
                                      
                                            <i class="fa fa-heart padge-l offerFav{{$latestProduct->id}}"
                                               data-id="{{$latestProduct->id}}"></i>
                                      
                                    @endif
                                  
                                        <img
                                            src="{{$latestProduct->default_image}}"
                                            class="img-responsive center-block" alt="">
                                        <h4>{{$latestProduct->title}}</h4>


                                        <p>
                                             <span>
                                                {{$latestProduct->price_after_discount}}
                                             </span>ريال

                                        @if($latestProduct->discount_percentage != 0)
                                            <p class="small-p">
                                                <del> {{$latestProduct->price}} </del>
                                                ريال
                                            </p>
                                            @endif

                                            </p>
                                </div>
                            @endforeach
                        @endif

                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- end recently arrived-->


                <!-- start most wanted -->
                <div class="special_offers">
            <div class="container">
                <div class="row">
                    <div class="col-xs-10">

                        <h3>الأكثر طلباً</h3>
                    </div>
                  
                    <div class="col-xs-12" style="direction: ltr;">
                        <div class="owl-carousel most owl-theme">
                            @if(isset($data['mostWanted']))
                                @foreach($data['mostWanted'] as $single_top_ordered)
                                    <div class="item">
                                        @if($single_top_ordered->discount_percentage != 0)
                                            <span class="padge-r">
                                    خصم {{$single_top_ordered->discount_percentage}}%
                                   </span>
                                        @endif

                                        @if(Auth::guard('customer')->check())
                                            <i class="fa fa-heart padge-l top-orderFav{{$single_top_ordered->id}}"
                                               style="color:{{$single_top_ordered->is_favourite != true ? "" : "red"}}"
                                               data-id="{{$single_top_ordered->id}}"></i>
                                        @else
                                            
                                                <i class="fa fa-heart padge-l top-orderFav{{$single_top_ordered->id}}"
                                                   
                                                   data-id="{{$single_top_ordered->id}}"></i>
                                            
                                        @endif

                                      
                                            <img
                                                src="{{$single_top_ordered->default_image }}"
                                                class="img-responsive center-block" alt="">
                                            <h4>{{$single_top_ordered->title}}</h4>

                                      
                                        <span>
                                            {{$single_top_ordered->price_after_discount}}

                                        </span> ريال</p>
                                                @if($single_top_ordered->discount_percentage != 0)
                                                    <p class="small-p">
                                                        <del> {{$single_top_ordered->price}} </del>
                                                        ريال
                                                    </p>
                                                @endif
                                          

                                    </div>
                                @endforeach
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- end most wanted -->

                <!-- start most viewd -->
                <div class="special_offers">
            <div class="container">
                <div class="row">
                    <div class="col-xs-10">

                        <h3>الأكثر مشاهدة</h3>
                    </div>
                  
                    <div class="col-xs-12" style="direction: ltr;">
                        <div class="owl-carousel most owl-theme">
                            @if(isset($data['mostViewed']))
                                @foreach($data['mostViewed'] as $single_top_ordered)
                                    <div class="item">
                                        @if($single_top_ordered->discount_percentage != 0)
                                            <span class="padge-r">
                                    خصم {{$single_top_ordered->discount_percentage}}%
                                   </span>
                                        @endif

                                        @if(Auth::guard('customer')->check())
                                            <i class="fa fa-heart padge-l top-orderFav{{$single_top_ordered->id}}"
                                               style="color:{{$single_top_ordered->is_favourite != true ? "" : "red"}}"
                                               data-id="{{$single_top_ordered->id}}"></i>
                                        @else
                                            
                                                <i class="fa fa-heart padge-l top-orderFav{{$single_top_ordered->id}}"
                                                   
                                                   data-id="{{$single_top_ordered->id}}"></i>
                                            
                                        @endif

                                      
                                            <img
                                                src="{{$single_top_ordered->default_image }}"
                                                class="img-responsive center-block" alt="">
                                            <h4>{{$single_top_ordered->title}}</h4>

                                      
                                        <span>
                                            {{$single_top_ordered->price_after_discount}}

                                        </span> ريال</p>
                                                @if($single_top_ordered->discount_percentage != 0)
                                                    <p class="small-p">
                                                        <del> {{$single_top_ordered->price}} </del>
                                                        ريال
                                                    </p>
                                                @endif
                                          

                                    </div>
                                @endforeach
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- end most viewd -->

                        <!-- start most viewd -->
                        <div class="special_offers">
            <div class="container">
                <div class="row">
                    <div class="col-xs-10">

                        <h3>Brands</h3>
                    </div>
                  
                    <div class="col-xs-12" style="direction: ltr;">
                        <div class="owl-carousel most owl-theme">
                          
                        @foreach($data["brands"] as $key => $brand)
                                    <div class="item">
                                
                                            <img
                                                src="/storage/{{$brand->image}}"
                                                class="img-responsive center-block" alt="">
                                            <h4>{{$brand->title}}</h4>
                                          

                                    </div>
                                @endforeach
                            
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- end most viewd -->

    <div class="clearfix"></div>

   

@endsection




