<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--    internet explorer compatibility meta-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--    first mobile meta-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task</title>
    <!-- icon -->
    <link rel="shortcut icon" href="/frontend/images/favicon.png">
    <!--    bootstrap-->
        <link rel="stylesheet" href="/frontend/css/bootstrap.min.css">
   
    <link rel="stylesheet" href="/frontend/css/owl.carousel.min.css">
    <!--link rel="stylesheet" href="/frontend/css/animate.css"-->
    <!-- lightbox -->
    <link rel="stylesheet" href="/frontend/css/lightbox.min.css">
    {{-- flags --}}
    <link rel="stylesheet" href="/frontend/css/flags.css">
    <!--    fontawesome-->
    <link rel="stylesheet" href="/frontend/css/font-awesome.min.css">
    <!--	opensasn fonts-->
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@700&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
    <!--    my stylesheet-->
    <link rel="stylesheet" href="/frontend/css/filter-style.css">
    <link rel="stylesheet" href="/frontend/css/style.css">
        <link rel="stylesheet" href="/frontend/css/style-en.css">
  

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.4/bluebird.min.js"></script>
    <script src="https://secure.gosell.io/js/sdk/tap.min.js"></script>

    @stack('after-styles')

    <script>
        var base_url = '{{ url('') }}';
    </script>
</head>
<body>
<div id="updateDiv">
    @include('frontend.includes.header')

{{--    @if(session('message'))--}}
{{--        <div class="alert alert-success">--}}
{{--            {{session('message')}}--}}
{{--        </div>--}}
{{--    @endif--}}

{{--    @if(session('error'))--}}
{{--        <div class="alert alert-danger">--}}
{{--            {{session('error')}}--}}
{{--        </div>--}}
{{--    @endif--}}


     @if(session('error'))
        <div class="alert alert-danger">
           {{session('error')}}
        </div>
    @endif

    <div style="display: none" class="alert alert-danger" id="err"></div>
    <div style="display:none" class="alert alert-success" id="succ"></div>

    @yield('content')

<!-- download -->


    <!-- start scroll button -->
    <div class="">
        <a data-scroll="" href="#bazinga" id="scroll-btn" class="btn btn-default">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>

    <!-- scroll button -->


    @include('frontend.includes.footer')

    @stack('before-scripts')

    <script src="/frontend/js/html5shiv.min.js"></script>
    <script src="/frontend/js/jquery-1.12.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="/frontend/js/owl.carousel.min.js"></script>
    <script src="/frontend/js/bootstrap-arabic.min.js"></script>
    <script src="/frontend/js/lightbox.min.js"></script>
    <script src="/frontend/js/wow.min.js"></script>
    <script src="/frontend/js/jquery.mixitup.min.js"></script>
    <script src="/frontend/js/jquery.countdown.min.js"></script>
    <script src="/frontend/js/jquery.flagstrap.min.js"></script>
    <script src="/frontend/js/main.js"></script>
  
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
   

    <script>
        $(document).ready(function () {
            $("body").on("click", function () {
                $(".itemList").css("display", "none");
                $("#search_item").val("");
            });


        });


    </script>
    <script>
        @if(Session::has('message'))
        swal.fire({
            icon: "{{Session::get('alert-type')}}",
            title: "{{Session::get('title')}}",
            text: "{{Session::get('message')}}",
            button: {
                text: "موافق",
            },
        });
        @endif
    </script>

    @stack('after-scripts')
</div>
</body>
</html>
