<!-- start header -->
<div class="header">
    <div class="top_header">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-4  logo"><a href="{{route('home')}}">
                      
                    </a></div>
                <div class="col-md-4 col-sm-4 search">
                    <div class="input-group btn-block">
                        <form action="">
                           

                            <input id="search_item" type="text" class="form-control btn-block" name="search_item"
                                   placeholder="بحث عن منتج"
                                   autocomplete="off">
                            <div id="itemList" class="itemList">
                            </div>

                            <button type="button" class="input-group-addon"><i class="glyphicon glyphicon-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    
                    
                        <div class="col-md-3 col-sm-3 col-xs-3 fav"><a href="#"><i
                                    class="fa fa-heart-o"></i></a></div>
                   
                   
                        <div class="cartheader">
                            <div class="col-md-3 col-sm-3 col-xs-3 cart dropdown"><i
                                    class="fa fa-shopping-cart dropbtn"></i>
                               
                        
                            </div>
                        </div>
                  

                    @if(Auth::guard('customer')->check())
                        <div class="col-md-1 col-sm-3 col-xs-3 login">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                     {{Auth::guard('customer')->user()->name}}
                                    <span class="caret"></span> 
                                    <i class="fa fa-user"></i>
                                </button>
                                <ul class="dropdown-menu login-drop" aria-labelledby="dropdownMenu1">
                                  
                                    <li><a href="#"> <i
                                                class="fa fa-sign-out"></i> logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @else
                        <div class="col-md-9 col-sm-3 col-xs-6 login"><i class="fa fa-user"></i>
                            <a href="#">login / </a> <a
                                href="#">register</a>
                        </div>
                    @endif
                </div>


            </div>
        </div>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        @php $categories= \App\Models\Categories\Category::parents()->get(); @endphp
        <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  
                    @foreach($categories as $category)
                        <li><a href="#">{{$category->name}}</a>
                        </li>
                    @endforeach
                </ul>


            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
</div>

<!-- end header -->
