<?php

namespace App\Models\Slides\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSlideRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';
        // rules

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.slides.'),
        ];
    }
}
