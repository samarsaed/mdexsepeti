<?php

namespace App\Models\Slides;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use QCod\ImageUp\HasImageUploads;

class Slide extends Model
{
    use SoftDeletes,HasImageUploads;

    protected $fillable = ['slide','order'];

    protected static $imageFields = ['slide'];

}


