<?php

namespace App\Models\Slides\Repositories;

use App\Models\Slides\Slide;
use app\Repositories\BaseRepository;
use App\Models\Slides\Repositories\SlideRepositoryInterface;
use App\Models\Slides\Repositories\ViewData;

class SlideRepository extends BaseRepository implements SlideRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Slide $slide)
    {
        $this->model = $slide;
    }


}

