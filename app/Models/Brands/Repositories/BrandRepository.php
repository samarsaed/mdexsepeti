<?php

namespace App\Models\Brands\Repositories;

use App\Models\Brands\Brand;
use App\Repositories\BaseRepository;
use App\Models\Brands\Repositories\BrandRepositoryInterface;
use App\Models\Brands\Repositories\ViewData;

class BrandRepository extends BaseRepository implements BrandRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Brand $brand)
    {
        $this->model = $brand;
    }


}

