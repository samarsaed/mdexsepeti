<?php

namespace App\Models\Brands;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;
    protected $fillable = ['title','image'];

    public function products()
    {
        return $this->hasMany('App\Models\Products\Product', 'Product_id');
    }

    

}


