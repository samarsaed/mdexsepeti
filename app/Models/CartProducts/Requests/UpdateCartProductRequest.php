<?php

namespace App\Models\CartProducts\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCartProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';
        // rules

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.cartproducts.'),
        ];
    }
}
