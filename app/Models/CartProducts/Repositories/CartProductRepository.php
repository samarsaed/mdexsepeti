<?php

namespace App\Models\CartProducts\Repositories;

use App\Models\CartProducts\CartProduct;
use App\Repositories\BaseRepository;
use App\Models\CartProducts\Repositories\CartProductRepositoryInterface;
use App\Models\CartProducts\Repositories\ViewData;

class CartProductRepository extends BaseRepository implements CartProductRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(CartProduct $cartproduct)
    {
        $this->model = $cartproduct;
    }


}

