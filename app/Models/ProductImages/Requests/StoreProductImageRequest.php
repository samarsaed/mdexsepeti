<?php

namespace App\Models\ProductImages\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreProductImageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';
        // rules

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.productimages.'),
        ];
    }
}
