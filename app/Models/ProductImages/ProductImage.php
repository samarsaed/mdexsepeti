<?php

namespace App\Models\ProductImages;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use QCod\ImageUp\HasImageUploads;
class ProductImage extends Model
{
    protected $table = 'product_images';

    use SoftDeletes,HasImageUploads;

    const DEFAULT_IMAGE = 1;
    const NO_DEFAULT_IMAGE = 0;

    protected $fillable = ['image','product_id','order','is_default'];

    protected static $imageFields = ['image'];

    public function product()
	{
		return $this->belongsTo('App\Models\Products\Product','product_id');
    }
}


