<?php

namespace App\Models\ProductImages\Repositories;

use App\Models\ProductImages\ProductImage;
use app\Repositories\BaseRepository;
use App\Models\ProductImages\Repositories\ProductImageRepositoryInterface;
use App\Models\ProductImages\Repositories\ViewData;

class ProductImageRepository extends BaseRepository implements ProductImageRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(ProductImage $productimage)
    {
        $this->model = $productimage;
    }


}

