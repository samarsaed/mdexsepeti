<?php

namespace App\Models\Carts\Repositories;

use App\Models\Carts\Cart;
use App\Repositories\BaseRepository;
use App\Models\Carts\Repositories\CartRepositoryInterface;
use App\Models\Carts\Repositories\ViewData;

class CartRepository extends BaseRepository implements CartRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Cart $cart)
    {
        $this->model = $cart;
    }


}

