<?php

namespace App\Models\Customers\Repositories;

use App\Models\Customers\Customer;
use app\Repositories\BaseRepository;
use App\Models\Customers\Repositories\CustomerRepositoryInterface;
use App\Models\Customers\Repositories\ViewData;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }


}

