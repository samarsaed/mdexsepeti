<?php


namespace App\Models\Customers;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

/**
 * @method create(array $array)
 */
class Customer extends Authenticatable
{
    use SoftDeletes, HasApiTokens, Notifiable;

    protected $fillable = ['name', 'email', 'phone'
         ,'verified', 'active',
        'verification_code', 'password', 'remember_token'];

        public function setPasswordAttribute($password): void
        {
            if ($password) {
                // If password was accidentally passed in already hashed, try not to double hash it
                if (
                    (\strlen($password) === 60 && preg_match('/^\$2y\$/', $password)) ||
                    (\strlen($password) === 95 && preg_match('/^\$argon2i\$/', $password))
                ) {
                    $hash = $password;
                } else {
                    $hash = Hash::make($password);
                }
    
                $this->attributes['password'] = $hash;
            }
        }
}


