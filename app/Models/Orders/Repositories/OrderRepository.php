<?php

namespace App\Models\Orders\Repositories;

use App\Models\Orders\Order;
use app\Repositories\BaseRepository;
use App\Models\Orders\Repositories\OrderRepositoryInterface;
use App\Models\Orders\Repositories\ViewData;

class OrderRepository extends BaseRepository implements OrderRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Order $order)
    {
        $this->model = $order;
    }


}

