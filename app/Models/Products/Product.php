<?php

namespace App\Models\Products;

use App\Models\OffersCategories\OffersCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductImages\ProductImage;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['brand_id','title','description', 'price', 'discount_percentage', 'price_after_discount', 'tag', 'no_views'];

    protected $hidden = ['pivot'];


    protected $appends = ["default_image"];

    const NORMAL_TAG = 1;
    const BEST_SELL_TAG = 2;
    const MOST_VIEWED_TAG = 3;


    public function getDefaultImageAttribute()
    {
        return $this->product_media()->where('is_default',ProductImage::DEFAULT_IMAGE)->first() ? $this->product_media()->where('is_default',ProductImage::DEFAULT_IMAGE)->first()->imageUrl('image') ?? config('app.url').'/frontend/images/no-image.jpg' : config('app.url').'/frontend/images/no-image.jpg';
    }

    public function product_media()
    {
        return $this->hasMany(ProductImage::class,'product_id')->orderBy('order');
    }

    public function cartItems()
    {
        return $this->hasMany('App\Models\CartProducts\CartProduct', 'item_id');
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderProducts\OrderProduct', 'item_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brands\Brand', 'brand_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Categories\Category', 'product_category', 'product_id', 'category_id')->withTimestamps();
    }

    public function wishlists()
    {
        return $this->belongsToMany('App\Models\Customers\Customer', 'wishlists', 'product_id', 'customer_id')->withTimestamps();
    }

    public function customer_whishlist()
    {
        return $this->wishlists()
            ->where('customer_id', auth()->id());
    }


    public function getIsFavouriteAttribute()
    {

        $relation = $this->customer_whishlist()->first();
        $is_favourite = $relation ? true : false;
        return $is_favourite;

    }

}


