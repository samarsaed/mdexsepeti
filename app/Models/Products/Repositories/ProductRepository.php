<?php

namespace App\Models\Products\Repositories;

use App\Models\Products\Product;
use App\Repositories\BaseRepository;
use App\Models\Products\Repositories\ProductRepositoryInterface;
use App\Models\Products\Repositories\ViewData;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function create($data)
    {

        $user = $this->model->create($data);

        if ($user) {

            return ["status" => true, "message" => __("api.success"), "data" => $user];
        }

        return ["status" => false, "message" => __("api.failed"), "data" => ""];

    }


}

