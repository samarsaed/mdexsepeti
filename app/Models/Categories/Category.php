<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use QCod\ImageUp\HasImageUploads;
class Category extends Model
{
    use SoftDeletes,HasImageUploads;

    protected $fillable = ['image','brand_id','name','order','parent_id'];
    
    protected static $imageFields = ['image'];

    public function brand()
    {
         return $this->belongsTo('App\Models\Brands\Brand','brand_id');
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function parent () {

        return $this->belongsTo(Category::class, 'parent_id');

    }

    public function scopeParents ($builder) {

        return $builder->where('parent_id', null);

    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Products\Product', 'product_category','category_id','product_id')->withTimestamps();
    }


}


