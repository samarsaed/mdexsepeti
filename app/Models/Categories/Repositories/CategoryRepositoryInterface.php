<?php

namespace App\Models\Categories\Repositories;

use Illuminate\Http\Request;
 
interface CategoryRepositoryInterface
{
public function getItems($id);

}

