<?php

namespace App\Models\Categories\Repositories;

use App\Models\Categories\Category;
use App\Repositories\BaseRepository;
use App\Models\Categories\Repositories\CategoryRepositoryInterface;
use App\Models\Categories\Repositories\ViewData;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Category $category)
    {
        $this->model = $category;
    }
    public function getItems($id)
    {
        $category=$this->model->where('id',$id)->first();

        if($category){
         $products=$category->products()->get();
         return ["status" => true, "message" => __("api.success"), "data" => $products];
        }

        return ["status" => false, "message" => __("category not found"), "data" => ""];
    }

}

