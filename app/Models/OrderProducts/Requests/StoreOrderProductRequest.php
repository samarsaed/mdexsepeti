<?php

namespace App\Models\OrderProducts\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';
        // rules

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.orderproducts.'),
        ];
    }
}
