<?php

namespace App\Models\OrderProducts\Repositories;

use App\Models\OrderProducts\OrderProduct;
use app\Repositories\BaseRepository;
use App\Models\OrderProducts\Repositories\OrderProductRepositoryInterface;
use App\Models\OrderProducts\Repositories\ViewData;

class OrderProductRepository extends BaseRepository implements OrderProductRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(OrderProduct $orderproduct)
    {
        $this->model = $orderproduct;
    }


}

