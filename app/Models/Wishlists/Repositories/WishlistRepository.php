<?php

namespace App\Models\Wishlists\Repositories;

use App\Models\Wishlists\Wishlist;
use app\Repositories\BaseRepository;
use App\Models\Wishlists\Repositories\WishlistRepositoryInterface;
use App\Models\Wishlists\Repositories\ViewData;

class WishlistRepository extends BaseRepository implements WishlistRepositoryInterface
{
    use ViewData;

//    protected $with = [''];

	public function __construct(Wishlist $wishlist)
    {
        $this->model = $wishlist;
    }


}

