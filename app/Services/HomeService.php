<?php

namespace App\Services;
use App\Models\Banners\Banner;
use App\Models\Categories\Repositories\CategoryRepositoryInterface;
use App\Models\Products\Repositories\ProductRepositoryInterface;
use App\Models\Slides\Repositories\SlideRepositoryInterface;
use App\Models\Brands\Repositories\BrandRepositoryInterface;
use DB;
class HomeService
{

    public function __construct(ProductRepositoryInterface $productRepository, CategoryRepositoryInterface $categoryRepository, SlideRepositoryInterface $slideRepository,BrandRepositoryInterface $brandRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->slideRepository = $slideRepository;
        $this->brandRepository = $brandRepository;
    }
    public function apiHome()
    {
        $offers = $this->productRepository
            ->where('discount_percentage', '0', '!=')
            ->with(['product_media','wishlists'])
            ->latest()->take(15)->get();
           
        $homeslides = $this->slideRepository->orderBy('order')->get();
        $latest = $this->productRepository->orderBy('created_at', 'DESC')->take(15)
        ->get();
        $mostWanted = DB::table('products')
        ->join('order_products','products.id', '=','order_products.item_id')
        ->join('orders','orders.id', '=','order_products.order_id')
        ->select('products.title','products.price','products.price_after_discount','products.discount_percentage','order_products.item_id', DB::raw('sum( order_products.quantity ) countSold'))
        ->orderBy('countSold','DESC')
        ->groupBy('order_products.item_id','products.title','products.price','products.price_after_discount','products.discount_percentage')
        ->take(9)
        ->get();

        $mostViewed = $this->productRepository->orderBy('no_views', 'DESC')->take(15)
        ->get();
       
        $brands = $this->brandRepository->orderBy('order')->take(15)
        ->get();

        $collection = collect([
            'offers' => $offers,
            'slides' => $homeslides,
            'latestProducts' => $latest,
            'mostWanted' => $mostWanted,
            'mostViewed' => $mostViewed,
            'brands'=> $brands,
        ]);

        return ["status" => true, "message" => __("api.home"), "data" => $collection];
    }
   

}
