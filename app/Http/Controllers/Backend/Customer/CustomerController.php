<?php

namespace App\Http\Controllers\Backend\Customer;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Customers\Repositories\CustomerRepository;
use App\Models\Customers\Requests\StoreCustomerRequest;
use App\Models\Customers\Requests\UpdateCustomerRequest;


class CustomerController extends CustomController
{
    protected $view = 'backend.customers';

    protected $route = 'admin.customers';

    protected $storeRequestFile = StoreCustomerRequest::class;

    protected $updateRequestFile = UpdateCustomerRequest::class;

    public function __construct(CustomerRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {

        $customers = Customer::query()->search(request()->get('search'))->latest()->paginate();

        return view('backend.customers.index', compact('customers'));

    }

}


