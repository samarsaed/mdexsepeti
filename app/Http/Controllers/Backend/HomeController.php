<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Services\HomeService;
class HomeController extends Controller
{

    private $homeService;
    public $webResponse;

    public function __construct(HomeService $homeService)
    {
        $this->homeService = $homeService;
      

    }


    public function frontHome()
    {
        $response = $this->homeService->apiHome();

        return view('frontend.home', ['data'=>$response['data']]);
    }
     

}
