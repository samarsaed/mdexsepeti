<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Categories\Repositories\CategoryRepositoryInterface;
use App\Models\Categories\Requests\StoreCategoryRequest;
use App\Models\Categories\Requests\UpdateCategoryRequest;
use App\Helpers\ApiResponse;


class CategoryController extends CustomController
{


    protected $storeRequestFile = StoreCategoryRequest::class;

    protected $updateRequestFile = UpdateCategoryRequest::class;

    private $categoryRepository;
    public $apiResponse;

    public function __construct(CategoryRepositoryInterface $categoryRepository,ApiResponse $apiResponse)
    {
        parent::__construct($categoryRepository);
        $this->categoryRepository = $categoryRepository;
        $this->apiResponse = $apiResponse;
    }

    public function getItems($id)
    {
        $response = $this->categoryRepository->getItems($id);

        return $this->apiResponse->responseHandler($response);

    }


}


