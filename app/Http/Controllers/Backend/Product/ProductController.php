<?php

namespace App\Http\Controllers\Backend\Product;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Products\Repositories\ProductRepositoryInterface;
use App\Models\Products\Requests\StoreProductRequest;
use App\Models\Products\Requests\UpdateProductRequest;
use App\Helpers\ApiResponse;

class ProductController extends CustomController
{

    protected $storeRequestFile = StoreProductRequest::class;

    protected $updateRequestFile = UpdateProductRequest::class;

    private $productRepository;
    public $apiResponse;


    public function __construct(ProductRepositoryInterface $productRepository,ApiResponse $apiResponse)
    {
        parent::__construct($productRepository);
        $this->productRepository = $productRepository;
        $this->apiResponse = $apiResponse;
    }



}


