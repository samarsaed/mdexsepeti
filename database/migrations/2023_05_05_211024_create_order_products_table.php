<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->unsignedBigInteger('item_id')->unsigned()->nullable();
            $table->foreign('item_id')->references('id')->on('products')->onDelete('cascade');
            $table->text("notes")->nullable();
            $table->double("subtotal",8,2)->default(0);
            $table->double("discount",8,2)->default(0);
            $table->double("quantity",8,2)->default(0);
            $table->double("total",8,2)->default(0); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
