<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 10; $i++) {
            \App\Models\Brands\Brand::create([
                'image' => 'uploads/brand.png',
                'title' => 'brand' . $i,
             
            ]);
        }

        for ($i = 1; $i <= 10; $i++) {
            \App\Models\Categories\Category::create([
                'image' => 'uploads/category.png',
                'parent_id' => $i%2 == 0 ? $i-1 : null,
                'order' => $i,
                'name' => 'category' . $i
               
            ]);
        }

        for ($i = 1; $i <= 10; $i++) {
            \App\Models\Products\Product::create([
                'brand_id' => $i,
                'discount_percentage'=> $i%2 == 0 ? 10 : 0,
                'price'=>50,
                'price_after_discount'=> $i%2 == 0 ? 30 : 50,
                'title' => 'product' . $i,
                'tag'=>rand(1,3),
                'no_views'=>$i+1,
                
            ]);
        }

        for ($i = 1; $i <= 10; $i++) {
            \App\Models\ProductImages\ProductImage::create([
                'product_id' => $i,
                'image'=>'uploads/product.png',
                'is_default'=>\App\Models\ProductImages\ProductImage::DEFAULT_IMAGE
            ]);
        }

        for ($i = 1; $i <= 5; $i++) {
            \App\Models\Slides\Slide::create([
                'slide'=>'uploads/side.png',
                'order'=>$i,
            ]);
        }

    }
}
