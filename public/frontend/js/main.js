/*global $, jQuery, alert*/
$(function () {
  "use strict";
  $(".add-address").click(function () {
    $(".address-content").toggle(1000);
  });

  $('.nav-tabs li a.first').click(function () {
    $('.nav-tabs li input.first').prop("checked", true);
});
$('.nav-tabs li a.sec').click(function () {
  $('.nav-tabs li input.sec').prop("checked", true);
});


});

$(".nav-tabs-wrapper a").click(function () {
  $(this).parent().addClass("btn-active").siblings().removeClass("btn-active");
});

$(".js-number").bind('keydown', function (e) {
  var targetValue = $(this).val();
  if (e.which === 8 || e.which === 13 || e.which === 37 || e.which === 39 || e.which === 46) {
    return;
  }

  if (e.which > 47 && e.which < 58 && targetValue.length < 2) {
    var c = String.fromCharCode(e.which);
    var val = parseInt(c);
    var textVal = parseInt(targetValue || "0");
    var result = textVal + val;

    if (result < 0 || result > 99) {
      e.preventDefault();
    }

    if (targetValue === "0") {
      $(this).val(val);
      e.preventDefault();
    }
  } else {
    e.preventDefault();
  }
});

$('.panel-collapse').on('show.bs.collapse', function () {
  $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
  $(this).siblings('.panel-heading').removeClass('active');
});

$('.owl-carousel').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  autoplay: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
});
$('.most').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  autoplay: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
});
$('.arrived').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  autoplay: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 3
    },
    1000: {
      items: 5
    }
  }
});
var scrollButton = $("#scroll-btn");
$(window).scroll(function () {
  if ($(this).scrollTop() >= 500) {
    scrollButton.fadeIn();
  } else {
    scrollButton.fadeOut();
  }
});
$('#scroll-btn').click(function () {
  $('html, body').animate({
    scrollTop: 0
  }, 700);
  return false;
});
/*-------------------
		Quantity change
	--------------------- */
var proQty = $('.pro-qty');
proQty.prepend('<span class="dec qtybtn">-</span>');
proQty.append('<span class="inc qtybtn">+</span>');
proQty.on('click', '.qtybtn', function () {
  var $button = $(this);
  var oldValue = $button.parent().find('input').val();
  if ($button.hasClass('inc')) {
    var newVal = parseFloat(oldValue) + 1;
  } else {
    // Don't allow decrementing below zero
    if (oldValue > 0) {
      var newVal = parseFloat(oldValue) - 1;
    } else {
      newVal = 0;
    }
  }
  $button.parent().find('input').val(newVal);
});

/*-------------------
		Radio Btn
	--------------------- */
$(".fw-size-choose .sc-item label, .pd-size-choose .sc-item label").on('click', function () {
  $(".fw-size-choose .sc-item label, .pd-size-choose .sc-item label").removeClass('active');
  $(this).addClass('active');
});

// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByClassName("cart-li");
var i;
for (i = 0; i < myNodelist.length; i++) {
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  myNodelist[i].appendChild(span);
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function () {
    var div = this.parentElement;
    div.style.display = "none";
  }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function (ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    document.getElementById("myUL").appendChild(li);
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  for (i = 0; i < close.length; i++) {
    close[i].onclick = function () {
      var div = this.parentElement;
      div.style.display = "none";
    }
  }
}

filterSelection("all")

function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

// Show filtered elements
function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

// Hide elements that are not selected
function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// Add active class to the current control button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
$('#myBtnContainer .btn').click(function () {
  $(this).addClass('active').siblings().removeClass('active');
});
$('.tab tablinks').click(function () {
  $(this).addClass('active').siblings().removeClass('active');
});